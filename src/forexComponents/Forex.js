import React, { Component } from "react";
import ForexDropdown from "./ForexDropdown";
import ForexInput from "./ForexInput";
import { Button } from "reactstrap";

class Forex extends Component {
  state = {
    amount: 0,
    baseCurrency: null,
    targetCurrency: null,
    convertedAmount: 0,
    code: ""
  };

  handleAmount = e => {
    this.setState({
      amount: e.target.value
    });
  };

  handleBaseCurrency = currency => {
    this.setState({
      baseCurrency: currency
    });
  };

  handleTargetCurrency = currency => {
    this.setState({
      targetCurrency: currency
    });
  };

  handleConvert = () => {
    const code = this.state.baseCurrency.code;

    fetch("https://api.exchangeratesapi.io/latest?base=" + code)
      .then(res => res.json())
      .then(res => {
        const targetCode = this.state.targetCurrency.code;
        const rate = res.rates[targetCode];
        this.setState({ convertedAmount: this.state.amount * rate });
        this.setState({ code: this.state.targetCurrency.code });
      });
  };

  render() {
    console.log(this.state.amount);

    return (
      <div style={{ width: "70%" }}>
        <h1 className="text-center my-5">Forex Calculator</h1>
        <div
          className="d-flex justify-content-around"
          style={{ margin: "0 200px" }}
        >
          <ForexDropdown
            label={"Base Currency"}
            onClick={this.handleBaseCurrency}
            currency={this.state.baseCurrency}
          />
          <ForexDropdown
            label={"Target Currency"}
            onClick={this.handleTargetCurrency}
            currency={this.state.targetCurrency}
          />
        </div>
        <div className="d-flex justify-content-around">
          <ForexInput
            label={"Amount"}
            placeholder={"Amount to convert"}
            onChange={this.handleAmount}
          />
          <Button color="info" onClick={this.handleConvert}>
            Convert
          </Button>
        </div>
        <div>
          <h1 className="text-center">{this.state.convertedAmount}</h1>
        </div>
      </div>
    );
  }
}

export default Forex;
