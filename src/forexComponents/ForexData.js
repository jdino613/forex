const currencies = [
  { currency: "Philippine Peso", code: "PHP" },
  { currency: "US Dollars", code: "USD" },
  { currency: "Euro", code: "EUR" },
  { currency: "Thai Baht", code: "THB" },
  { currency: "South Korean Won", code: "KRW" },
  { currency: "Japanese Yen", code: "JPY" },
  { currency: "Indian Rupee", code: "INR" }
];

export default currencies;
