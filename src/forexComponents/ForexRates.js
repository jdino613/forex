import React, { Component } from "react";
import Currencies from "./ForexData";

class ForexRates extends Component {
  state = {
    currency: null,
    rate: 0
  };

  renderTable = () => {
    {
      Currencies.map((currency, index) => {
        return (
          <tr key={index}>
            <td>{currency.currency}</td>
            <td>{currency.code}</td>
          </tr>
        );
      });
    }
  };

  render() {
    return (
      <div className="d-flex justify-content-center align-items-center">
        <table>
          <thead>
            <td className="text-center" style={{ width: "300px" }}>
              Currency
            </td>
            <td className="text-center" style={{ width: "300px" }}>
              Rate
            </td>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}

export default ForexRates;
